﻿using System;

namespace FoxTales.Infrastructure.DTOFramework.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public sealed class GridHiddenColumnAttribute : Attribute
    {
    }
}
