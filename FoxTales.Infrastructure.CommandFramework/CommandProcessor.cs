﻿using System.Threading.Tasks;

namespace FoxTales.Infrastructure.CommandFramework
{
    public static class CommandProcessor
    {
        public static void StartInParallel(params CommandTask[] tasks)
        {
            Parallel.ForEach(tasks, t => t.Execute());
        }
    }
}