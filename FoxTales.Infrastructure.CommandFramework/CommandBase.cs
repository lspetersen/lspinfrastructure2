﻿using System;
using System.Transactions;
using Autofac;
using FoxTales.Infrastructure.DependencyInjection;

namespace FoxTales.Infrastructure.CommandFramework
{
    public abstract class AbstractCommand
    {
        internal bool IsChained { get; set; }
        private readonly ILifetimeScope _lifetimeScope;
        private IsolationLevel _isolationLevel;

        internal AbstractCommand()
        {
            _lifetimeScope = AutofacBootstrapper.BeginLifetimeScopeByApplicationType();
        }

        internal void Run(IsolationLevel isolationLevel)
        {
            Run(isolationLevel, _lifetimeScope);
        }

        internal void Run(IsolationLevel isolationLevel, ILifetimeScope lifetimeScope)
        {
            _isolationLevel = isolationLevel;

            TransactionScope scope = null;
            if (!IsChained)
            {
                scope = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions { IsolationLevel = isolationLevel });
            }

            try
            {
                PerformCommand(lifetimeScope);
                if (scope != null)
                {
                    scope.Complete();
                }
            }
            finally
            {
                if (scope != null)
                {
                    scope.Dispose();
                }
            }
        }
        
        protected void StartChildCommand(CommandBase cmd)
        {
            cmd.IsChained = true;
            cmd.Run(_isolationLevel);
        }

        protected T StartChildCommand<T>(CommandBase<T> cmd)
        {
            cmd.IsChained = true;
            return cmd.Execute(_isolationLevel);
        }

        protected abstract void PerformCommand(ILifetimeScope lifetimeScope);
    }

    public abstract class CommandBase : AbstractCommand
    {
        public void Execute(IsolationLevel isolationLevel)
        {
            Run(isolationLevel);
        }

        protected abstract void OnExecuting(ILifetimeScope lifetimeScope);
        
        public CommandTask ToCommandTask(IsolationLevel isolationLevel)
        {
            return CommandTask.Create(isolationLevel, this);
        }

        protected override void PerformCommand(ILifetimeScope lifetimeScope)
        {
            OnExecuting(lifetimeScope);
        }
    }

    public abstract class CommandBase<T> : AbstractCommand
    {
        private T _result;

        public T Execute(IsolationLevel isolationLevel)
        {
            Run(isolationLevel);
            return _result;
        }

        protected abstract T OnExecuting(ILifetimeScope lifetimeScope);

        protected override void PerformCommand(ILifetimeScope lifetimeScope)
        {
            _result = OnExecuting(lifetimeScope);
        }
    }
}
