﻿// Copyright (C) 2014 FoxTales
// Released under the MIT License

namespace FoxTales.Infrastructure.DomainFramework
{
    public abstract class ValueBase : ObjectBase
    {
        public override void MarkModification()
        { }
    }
}
