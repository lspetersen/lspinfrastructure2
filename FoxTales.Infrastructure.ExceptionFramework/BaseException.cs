﻿using System;
using System.Runtime.Serialization;

namespace FoxTales.Infrastructure.ExceptionFramework
{
    [Serializable]
    public class BaseException<TErrorCode> : Exception where TErrorCode : struct, IConvertible, IComparable, IFormattable
    {
        public TErrorCode ErrorCode { get; private set; }

        public BaseException(string message, Exception innerException, TErrorCode errorCode) : base(message, innerException)
        {
            CheckType();
            ErrorCode = errorCode;
        }

        public BaseException(Exception innerException, TErrorCode errorCode) : base("", innerException)
        {
            CheckType();
            ErrorCode = errorCode;
        }

        public BaseException(TErrorCode errorCode) : base("")
        {
            CheckType();
            ErrorCode = errorCode;
        }

        protected BaseException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        private void CheckType()
        {
            //https://msdn.microsoft.com/en-us/library/ms229007(v=vs.110).aspx
            //X DO NOT throw System.Exception or System.SystemException.
            if (!typeof(TErrorCode).IsEnum)
                throw new Exception("ErrorCode must be an enum");
        }
    }
}
